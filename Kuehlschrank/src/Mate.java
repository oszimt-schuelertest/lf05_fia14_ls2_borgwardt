
public class Mate extends Getraenke{

	public Mate(String name) {
		super(name, 500);
		
	}

	@Override
	public boolean essen() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean trinken() {
		this.menge -= 100;
		if(this.menge < 0)
			this.menge = 0;
		return true;
	}
	
	@Override
	public String status() {
		return  "Klasse: Mate, Instanz: "+this.name+", Menge: "+this.menge+"ml";
	}

}
