
public class Brot extends Speise {

	public Brot(int ID, int menge) {
		super("Wei�brot", menge);
		switch (ID) {
		case 0: this.name = "Wei�brot"; break;
		case 1: this.name = "Schwarzbrot"; break;
		case 2: this.name = "Mischbrot"; break;
		default: this.name = "Spezialbrot"; break;
		}
	}

	@Override
	public boolean essen() {
		this.menge -= 50;
		if(this.menge < 0)
			this.menge = 0;
		return true;
	}

	@Override
	public boolean trinken() {
		return false;
	}

	@Override
	public String status() {
		return  "Klasse: Brot, Instanz: "+this.name+", Menge: "+this.menge+"g";
	}
}
