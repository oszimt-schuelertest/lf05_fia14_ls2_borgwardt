
public class Wasser extends Getraenke {

	public Wasser(String name, int menge) {
		super(name, menge);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean essen() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean trinken() {
		this.menge -= 200;
		if(this.menge < 0)
			this.menge = 0;
		return true;
	}
	
	@Override
	public String status() {
		return  "Klasse: Wasser, Instanz: "+this.name+", Menge: "+this.menge+"ml";
	}

}
