
public class Wurst extends Speise {

	public Wurst(String name, int menge) {
		super(name, menge);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean essen() {
		this.menge -= 10;
		if(this.menge < 0)
			this.menge = 0;
		return true;
	}

	@Override
	public boolean trinken() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public String status() {
		return  "Klasse: Wurst, Instanz: "+this.name+", Menge: "+this.menge+"g";
	}

}
