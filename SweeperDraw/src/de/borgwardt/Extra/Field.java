package de.borgwardt.Extra;

import java.awt.Graphics2D;
import java.awt.Image;

public class Field {

	private int blockX;
	private int blockY;
	private String blockType;
	private boolean isOpen;
	private boolean isFLagged;
	
	
	private Field() {
		this.blockType = "Block";
		this.isOpen = false;
		this.isFLagged = false;
	}
	
	public Field(int x, int y) {
		this();
		this.blockX = x;
		this.blockY = y;
	
	}
	
	
	public void drawBlock(Graphics2D g) {
		Image image = null;	
		if(isFLagged) {
			image = Images.FLAG.image(false);
		}else if(!isOpen) {
			image = Images.BLOCKED.image(false);
		}else if(isOpen && blockType == "Danger" && !isFLagged){
			image = Images.DANGERBLOCK.image(false);
		}else if(!isFLagged) {
			switch (blockType) {
			case "Block": image = Images.BLOCKED.image(false); break;
			case "Danger": image = Images.BLOCKED.image(false); break;
			case "Empty": image = Images.EMPTY.image(false);break;
			case "0": image = Images.EMPTY.image(false); break;
			case "1": image = Images.ONE.image(false); break;
			case "2": image = Images.TWO.image(false); break;
			case "3": image = Images.THREE.image(false); break;
			case "4": image = Images.FOUR.image(false); break;
			case "5": image = Images.FIVE.image(false); break;
			case "6": image = Images.SIX.image(false); break;
			case "7": image = Images.SEVEN.image(false); break;
			case "8": image = Images.EIGHT.image(false); break;
			case "FLAG": image = Images.FLAG.image(false); break;
			default: break;
			}
		}
		g.drawImage(image, blockX,blockY,null);
	
	}
	public boolean isFLagged() {
		return isFLagged;
	}
	public void setFLagged(boolean isFLagged) {
		this.isFLagged = isFLagged;
	}
	
	public boolean isOpen() {
		return isOpen;
	}
	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	
	public String getBlockType() {
		return blockType;
	}
	public void setBlockType(String blockType) {
		this.blockType = blockType;
	}
}
