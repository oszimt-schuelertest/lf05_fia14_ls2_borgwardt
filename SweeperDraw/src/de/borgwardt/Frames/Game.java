package de.borgwardt.Frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Game extends JFrame implements ActionListener {
	private static JPanel MainPanel = new JPanel();
	private static int HOEHE ;
	private static int BREITE;
	static JButton buttonMitte;
	private static Game theGameInstance;

	

	private Game() {
		setupFrame();
		

	}

	

	private void setupFrame() {
		Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
		// set size of JFrame
		this.setPreferredSize(new Dimension(DimMax));
		this.setMinimumSize(new Dimension(DimMax));
		this.setMaximumSize(new Dimension(DimMax));
		
		// start JFrame maximised
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		// close JFrame 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setUndecorated(true);
		this.setLocationRelativeTo(null);
		HOEHE = this.getHeight();
		BREITE = this.getWidth();
		MainPanel.setLayout(new BorderLayout());

		setupMainMenu();

		this.add(MainPanel);
		this.setVisible(true);
	}
	
	private void setupMainMenu() {
		JPanel pnLinks = new JPanel();
		pnLinks.setPreferredSize(new Dimension(((BREITE-HOEHE)/2), 0));
		pnLinks.setBackground(new Color(224,205,148));
		JPanel pnRechts = new JPanel();
		pnRechts.setPreferredSize(new Dimension(((BREITE-HOEHE)/2), 0));
		pnRechts.setBackground(new Color(224,205,148));
		buttonMitte = new JButton("Click");
		buttonMitte.addActionListener(this);
		MainPanel.add(buttonMitte, BorderLayout.CENTER);
		MainPanel.add(pnLinks, BorderLayout.WEST);
		MainPanel.add(pnRechts, BorderLayout.EAST);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonMitte) {
			GameBoardSettings.getTheInsance();
			GameBoardSettings.getTheInsance().requestFocus();
		}

	}

	public static int getHOEHE() {
		return HOEHE;
	}

	public static int getBREITE() {
		return BREITE;
	}

	public static void gameLost() {
		theGameInstance.removeAll();
	}

	public static Game getTheGameInstance() {
		if(theGameInstance == null) 
			theGameInstance = new Game();
		return theGameInstance;
	}


	public static JPanel getMainPanel() {
		return MainPanel;
	}

	public static void setMainPanel(JPanel mainPanel) {
		MainPanel = mainPanel;
	}
	
	public static JButton getButtonMitte() {
		return buttonMitte;
	}

}
