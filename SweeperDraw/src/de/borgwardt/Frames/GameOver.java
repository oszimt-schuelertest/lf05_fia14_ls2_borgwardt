package de.borgwardt.Frames;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import de.borgwardt.Extra.Images;

@SuppressWarnings("serial")
public class GameOver extends JPanel{
private static GameOver theGameOverInstance;
private static JFrame Window = new JFrame();
	public GameOver() {
		setupFrame();

	}

	private void setupFrame() {
		Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
		Window.setPreferredSize(new Dimension(DimMax.width / 2,DimMax.height/2));
		Window.setMinimumSize(new Dimension(DimMax.width / 2,DimMax.height/2));
		Window.setMaximumSize(new Dimension(DimMax.width / 2,DimMax.height/2));
		Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Window.setResizable(false);
		Window.setUndecorated(true);
		Window.setLocationRelativeTo(null);
		Window.setBackground(new Color(1.0f,1.0f,1.0f,0f));
		
		
		this.setBackground(new Color(1.0f,1.0f,1.0f,0f));
		
	
	
		Window.add(this);
		Window.setVisible(true);
	}
	
	public static GameOver getTheGameOverInstance() {
		if(theGameOverInstance == null)
			theGameOverInstance = new GameOver();
		return theGameOverInstance;
	}
	
	@Override
		protected void paintComponent(Graphics g) {
			
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D) g;
			g2d.drawImage(Images.SIGN.image(true),0,0,null);
		}
	
}
