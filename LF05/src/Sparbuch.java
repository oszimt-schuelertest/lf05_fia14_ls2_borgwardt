
public class Sparbuch {

	private int kontoNummer;
	private double kapital;
	private double zinssatz;

	public void zahleEin(int betrag) {
		this.setKapital(getKapital() + betrag);
	}

	public void hebeAb(int betrag) {
		this.setKapital(getKapital() - betrag);
	}

	public void verzinse() {
		this.setKapital(getKapital() * ((getZinssatz() / 100) + 1));
	}

	public double getErtrag(double laufzeit) {
		double ertrag = this.getKapital();
		for (int i = 0; i < laufzeit; i++) {
			ertrag = getKapital() + (getKapital() / 100 * this.getZinssatz());
		}
		return ertrag;
	}

	public int getKontoNummer() {
		return kontoNummer;
	}

	public void setKontoNummer(int kontoNummer) {
		this.kontoNummer = kontoNummer;
	}

	public double getKapital() {
		return kapital;
	}

	public void setKapital(double kapital) {
		this.kapital = kapital;
	}

	public double getZinssatz() {
		return zinssatz;
	}

	public void setZinssatz(double zinssatz) {
		this.zinssatz = zinssatz;
	}

	public Sparbuch(int kontonummer, double kapital, double zins) {
		this.setKontoNummer(kontonummer);
		this.setKapital(kapital);
		this.setZinssatz(zins);
	}

}
