
public class Kiste {

	public double hoehe;
	public double breite;
	public double tiefe;
	public String farbe;
	
	
	
	
	public Kiste() {
		this.hoehe = 10;
		this.breite = 20;
		this.tiefe = 10;
		this.farbe = "braun";
		
	}
	public Kiste(double hoehe, double breite, double tiefe, String farbe) {
		this.hoehe = hoehe;
		this.breite = breite;
		this.tiefe = tiefe;
		this.farbe = farbe;
	}
	
	
	public double getVolumen() {
		double volumen = this.hoehe * this.breite * this.tiefe;
		return volumen;
	}
	
	public String getFarbe() {
		return this.farbe;
	}
}
