package de.borgwardt.Raumschiffe;

import java.util.ArrayList;

public class Raumschiff {
int photonTorpedoAnzahl;
int energieVersorgungInProzent;
int schildeInProzent;
int huelleInProzent;
int lebenserhaltungsSystemeInProzent;
int androidenAnzahl;
String schiffsName;
ArrayList<Ladung> ladungsVerzeichnis;
ArrayList<String> broadCastKommunikator;
static ArrayList<String> logBuch = new ArrayList<>();

public Raumschiff() {
	this.photonTorpedoAnzahl = 0;
	this.energieVersorgungInProzent = 100;
	this.schildeInProzent = 100;
	this.huelleInProzent = 100;
	this.lebenserhaltungsSystemeInProzent = 100;
	this.androidenAnzahl = 0;
	this.schiffsName = "no name";
	this.ladungsVerzeichnis = new ArrayList<>();
	this.broadCastKommunikator = new ArrayList<>();
	
}

/**
 * 
 * @param photonTorpedoAnzahl
 * @param energieVersorgungInProzent
 * @param schildeInProzent
 * @param huelleInProzent
 * @param lebenserhaltungsSystemeInProzent
 * @param androidenAnzahl
 * @param schiffsName
 */
public Raumschiff(int photonTorpedoAnzahl, int energieVersorgungInProzent,int schildeInProzent, int huelleInProzent, 
		int lebenserhaltungsSystemeInProzent, int androidenAnzahl, String schiffsName) {
	this();
	this.photonTorpedoAnzahl = photonTorpedoAnzahl;
	this.energieVersorgungInProzent = energieVersorgungInProzent;
	this.schildeInProzent = schildeInProzent;
	this.huelleInProzent = huelleInProzent;
	this.lebenserhaltungsSystemeInProzent = lebenserhaltungsSystemeInProzent;
	this.androidenAnzahl = androidenAnzahl;
	this.schiffsName = schiffsName;
}

public void addLadung(Ladung neueLadung) {
	this.ladungsVerzeichnis.add(neueLadung);
}

public void photonTorpedoSchiessen(Raumschiff raumschiff, Raumschiff angegriffensRaumschiff) {
	if(this.photonTorpedoAnzahl > 0) {
		treffer(angegriffensRaumschiff);
		this.photonTorpedoAnzahl -= 1;
	}
}

public void phaserKanonenSchiessen(Raumschiff raumschiff) {
	
}

private void treffer(Raumschiff raumschiff) {
	if(raumschiff.schildeInProzent <= 0) {
		if(raumschiff.huelleInProzent <= 0) {
			if(raumschiff.lebenserhaltungsSystemeInProzent <= 0) {
				System.out.println("tot");
			}else if(raumschiff.lebenserhaltungsSystemeInProzent > 0) {
				raumschiff.lebenserhaltungsSystemeInProzent -= 10;
				System.out.println("Du hast dem " + raumschiff.getSchiffsName()+" 10 Schaden auf das Lebenserhaltungssystem gegeben");
			}
		}else if(raumschiff.huelleInProzent > 0) {
			raumschiff.huelleInProzent -= 10;
			System.out.println("Du hast dem " + raumschiff.getSchiffsName()+" 10 Schaden auf die Huelle gegeben");
		}
	}else if(raumschiff.schildeInProzent > 0) {
		raumschiff.schildeInProzent -= 10;
		System.out.println("Du hast dem " + raumschiff.getSchiffsName()+" 10 Schaden auf das Schild gegeben");
	}
}

public void nachrichtAnAlle(String nachricht, Raumschiff raumschiff) {
	this.broadCastKommunikator.add(nachricht);
	logBuch.add("\nRaumschiff: " + raumschiff.getSchiffsName() + " schreibt:" +nachricht);
}

public static ArrayList<String> eintraegeLogbuchZurueckgeben(){
	return logBuch;
}




public int getPhotonTorpedoAnzahl() {
	return photonTorpedoAnzahl;
}
public void setPhotonTorpedoAnzahl(int photonTorpedoAnzahl) {
	this.photonTorpedoAnzahl = photonTorpedoAnzahl;
}
public int getEnergieVersorgungInProzent() {
	return energieVersorgungInProzent;
}
public void setEnergieVersorgungInProzent(int energieVersorgungInProzent) {
	this.energieVersorgungInProzent = energieVersorgungInProzent;
}
public int getSchildeInProzent() {
	return schildeInProzent;
}
public void setSchildeInProzent(int schildeInProzent) {
	this.schildeInProzent = schildeInProzent;
}
public int getHuelleInProzent() {
	return huelleInProzent;
}
public void setHuelleInProzent(int huelleInProzent) {
	this.huelleInProzent = huelleInProzent;
}
public int getLebenserhaltungsSystemeInProzent() {
	return lebenserhaltungsSystemeInProzent;
}
public void setLebenserhaltungsSystemeInProzent(int lebenserhaltungsSystemeInProzent) {
	this.lebenserhaltungsSystemeInProzent = lebenserhaltungsSystemeInProzent;
}
public int getAndroidenAnzahl() {
	return androidenAnzahl;
}
public void setAndroidenAnzahl(int androidenAnzahl) {
	this.androidenAnzahl = androidenAnzahl;
}
public String getSchiffsName() {
	return schiffsName;
}
public void setSchiffsName(String schiffsName) {
	this.schiffsName = schiffsName;
}

}
