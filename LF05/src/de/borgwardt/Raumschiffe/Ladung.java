package de.borgwardt.Raumschiffe;

public class Ladung {
String name;
int menge;

public Ladung(String name, int menge) {
	super();
	this.name = name;
	this.menge = menge;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getMenge() {
	return menge;
}

public void setMenge(int menge) {
	this.menge = menge;
}



}
