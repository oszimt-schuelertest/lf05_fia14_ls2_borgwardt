package de.borgwardt.Raumschiffe;

import java.util.Scanner;

public class starteGalaxySpektakel {
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IKW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");

		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));

		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));

		Raumschiff aktivesRaumschiff;
		eingabeLoop: while (true) {
			System.out.print("\nW�hlen Sie ein Raumschiff aus\n\t1. " + klingonen.getSchiffsName() + "\n\t2. "
					+ romulaner.getSchiffsName() + "\n\t3. " + vulkanier.getSchiffsName() + "\nIhre Wahl:");
			String raumschiff = eingabe.next();
			switch (raumschiff) {
			case "1":
				aktivesRaumschiff = klingonen;
				break eingabeLoop;
			case "2":
				aktivesRaumschiff = romulaner;
				break eingabeLoop;
			case "3":
				aktivesRaumschiff = vulkanier;
				break eingabeLoop;
			default:
				System.out.println("Falsche eingabe!");
				break;
			}

		}

		while (true) {
			String nachricht;
			System.out.print(
					"\nBitte geben Sie eine 1 oder 2 f�r eine Aktion ein\n\t1. Angreifen\n\t2. Nachricht an alle\n: ");
			String aktion = eingabe.next();
			switch (aktion) {
			case "1":
				attackLoop: while (true) {
					System.out.print("\nW�hlen Sie ein Raumschiff zum angreifen aus\n\t1. " + klingonen.getSchiffsName()
							+ "\n\t2. " + romulaner.getSchiffsName() + "\n\t3. " + vulkanier.getSchiffsName());
					String raumSchiffAngreifen = eingabe.next();

					switch (raumSchiffAngreifen) {
					case "1":
						if (aktivesRaumschiff == klingonen) {
							System.out.println("Du versuchst dich selber anzugreifen");
						} else {
							aktivesRaumschiff.photonTorpedoSchiessen(aktivesRaumschiff, klingonen);
							break attackLoop;
						}
					break;
			
					case "2":
						if (aktivesRaumschiff == romulaner) {
							System.out.println("Du versuchst dich selber anzugreifen");
						} else {
							aktivesRaumschiff.photonTorpedoSchiessen(aktivesRaumschiff, romulaner);
							break attackLoop;
						}
						break;
					case "3":
						if (aktivesRaumschiff == vulkanier) {
							System.out.println("Du versuchst dich selber anzugreifen");
						} else {
							aktivesRaumschiff.photonTorpedoSchiessen(aktivesRaumschiff, vulkanier);
							break attackLoop;
						}
						break;
					default:
						System.out.println("falsche Eingabe");
					}
					break;
				}
				break;
			case "2":
				System.out.print("\nGeben sie bitte die Nachricht ein: ");
				nachricht = eingabe.nextLine();
				aktivesRaumschiff.nachrichtAnAlle(nachricht,aktivesRaumschiff);

				break;
			case "3":
				System.out.println("Logbuch:");
				System.out.println( Raumschiff.eintraegeLogbuchZurueckgeben());
				break;
			}
			if (klingonen.lebenserhaltungsSystemeInProzent <= 0 || romulaner.lebenserhaltungsSystemeInProzent <= 0
					|| vulkanier.lebenserhaltungsSystemeInProzent <= 0) {
				break;
			}
		}
	}
}
