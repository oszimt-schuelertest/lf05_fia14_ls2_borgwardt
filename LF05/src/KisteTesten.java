
public class KisteTesten {

	public static void main(String[] args) {
		Kiste eins = new Kiste(10, 20, 10, "blau");
		Kiste zwei = new Kiste(20, 40, 12, "gruen");
		Kiste drei = new Kiste(5, 10, 5, "rot");
		
		System.out.println("Volumen der ersten Kiste: " + eins.getVolumen() + " \tFarbe der ersten kiste: " + eins.getFarbe());
		System.out.println("Volumen der zweiten Kiste: " + zwei.getVolumen() + " \tFarbe der zweiten kiste: " + zwei.getFarbe());
		System.out.println("Volumen der dritten Kiste: " + drei.getVolumen() + " \tFarbe der dritten kiste: " + drei.getFarbe());
	}

}
