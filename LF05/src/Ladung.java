
public class Ladung {
	private String bezeichnung;
	private int menge;

	// getter fuer Bezeichnung
	public String getBezeichnung() {
		return this.bezeichnung;
	}

	// getter fuer Menge
	public int getMenge() {
		return this.menge;
	}

	// setter fuer Bezeichnung
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	// setter fuer Menge
	public void setMenge(int menge) {
		this.menge = menge;
	}
}
